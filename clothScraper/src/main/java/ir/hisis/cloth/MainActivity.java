package ir.hisis.cloth;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;

import ir.hisis.cloth.Utils.UtilUser;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.model.RestError;
import retrofit.client.Response;

public class MainActivity extends ActionBarActivity     {

    SlidingTabLayout tabsP;
    int Numboftabs = 3;
    MenuItem login_text;
    private TabsPagerAdapter mAdapter;
    private ViewPager viewPager;
    private ActionBar actionBar;
    private static String TAG = MainActivity.class.toString();

    CharSequence Titles[] = { "تمامی", "برترین ها", "مورد علاقه" };
    private TabsPagerAdapter adapter;
    private ViewPager pager;
    private SlidingTabLayout tabs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "before setting content view");
        setContentView(R.layout.activity_main);

        Log.i(TAG, "set content view");
        if(!UtilUser.isAuthenticated())
            if(UtilUser.isRegistered(getApplication()))
                tryToLogin(savedInstanceState);
        setupUI();
	}

    private void tryToLogin(final Bundle savedInstanceState){
        UtilUser.loginWithSharePreferences(getApplication(), new RestCallback<Boolean>() {
            @Override
            public void success(Boolean aBoolean, Response response) {
            }

            @Override
            public void failure(RestError error) {
            }
        });
    }

    private void setupUI(){
        Log.i(TAG, "setting up ui");
        setContentView(R.layout.activity_main);
        setTitle("");

        adapter =  new TabsPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarmain);
        setSupportActionBar(toolbar);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tab_light);
            }
        });

        setSlidingTabListenersPageChangeListener();
        tabs.setViewPager(pager);

    }

    private void setSlidingTabListenersPageChangeListener() {
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (!UtilUser.isRegistered(getApplication()) && position == 2) {

                    new registerDialog().show(getSupportFragmentManager(), "tagesh");
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.w(TAG, "page selected: " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.w(TAG, "pagescrolledstatechanged: " + state);
            }
        });
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
        login_text = menu.findItem(R.id.viewusername);
        login_text.setTitle("پویا");
        return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
			case(R.id.action_explore):
                reportUiOpenedTracker("explore");
				startActivity(new Intent(getApplicationContext(), ExpandableListViewActivity.class));
				return true;
			case(R.id.action_settings):
                reportUiOpenedTracker("settings");
				startActivity(new Intent(getApplicationContext(), CategoryActivity.class));
				return true;
			}
		return super.onOptionsItemSelected(item);

	}

    private void reportUiOpenedTracker(String name) {
        clothApplication.tracker().setScreenName(getClass().getSimpleName());
        clothApplication.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel(name)
                .build());
    }

    public static class registerDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            reportLoginTracker("-showed-");
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            setupDialogParameters(builder);
            return builder.create();
        }

        private void setupDialogParameters(AlertDialog.Builder builder) {
            builder.setMessage(R.string.you_should_login_to_access_this)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            reportLoginTracker("ok");
                            Intent intent = new Intent(getActivity().getApplicationContext(), LogInPageActivity.class);
                            startActivity(intent);

                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            reportLoginTracker("cancel");
                        }
                    });
        }

        protected void reportLoginTracker(String name) {
            clothApplication.tracker().setScreenName(getClass().getSimpleName());
            clothApplication.tracker().send(new HitBuilders.EventBuilder("login_massage", "SHOWED")
                    .setLabel(name)
                    .build());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        super.onStop();
    }
}
