package ir.hisis.cloth;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ir.hisis.cloth.Utils.Utils;
import ir.hisis.cloth.rest.RestClient;
import ir.hisis.cloth.rest.model.ImageResponse.Image;
import ir.hisis.cloth.rest.model.Message;
import ir.hisis.cloth.rest.service.ClothServer;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by pooyafayyaz on 4/26/2015.
 */ public class ImageAdapter extends BaseAdapter  {


    private int viewSize = 10;

    private static final int ANIMATED_ITEMS_COUNT = 2;

    private int lastAnimatedPosition = -1;
    private int itemsCount = 0;
    private boolean animateItems = false;


    private ArrayList<Image> images;

    private Context mContext;

    protected static final long DOUBLE_CLICK_MAX_DELAY = 1000L;

    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);

    private final Map<ViewHolder, AnimatorSet> likeAnimations = new HashMap<>();

    private final ArrayList<Integer> likedPositions = new ArrayList<>();

    private long thisTime = 0;
    private long prevTime = 0;
    private boolean firstTap = true;
    private String tap_item;
    //ViewHolder tag;
    public String TAG = ImageAdapter.class.toString();
    Activity activity;

    static class ViewHolder {
        Image imageObject;
        ImageView img;
        ImageView likeImg;
        View animateLikeImage;
        ImageView animateLikeBackground;
        ProgressBar progressBar;
    }

    public ImageAdapter(Activity a) {
        activity = a;
        mContext = activity.getApplicationContext();
        images = new ArrayList<>();
    }

    public void addImage(ArrayList<Image> images){
        this.images.addAll(images);
        this.notifyDataSetChanged();
    }

    public void resetImages(ArrayList<Image> images)
    {
        this.images = images;
        this.notifyDataSetChanged();
    }

    public int getCount() {
        return images.size();
    }

    public Image getItem(int position) {
        return images.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View imageItem;
        if (convertView == null) {
            imageItem = inflateAndSetupImageItem(position);
        }else {
            imageItem = convertView;
            ((ViewHolder) (imageItem.getTag())).progressBar.setVisibility(View.VISIBLE);
        }
        setImageResourcesForImageItem(position, imageItem);
        return imageItem;

    }

    private void setImageResourcesForImageItem(int position, View imageItem) {
        ViewHolder tag = (ViewHolder) imageItem.getTag();
        ImageView img = tag.img;
        Log.w(TAG, "loading this url: " + images.get(position).getUrl());
        Picasso.with(mContext).load(images.get(position).getUrl()).placeholder(R.drawable.place_holder).into(img, new ImageLoadedCallback(tag.progressBar) {
            @Override
            public void onSuccess() {
                if (this.progressBar != null) {
                    this.progressBar.setVisibility(View.GONE);
                }
            }
        });
       // Picasso.with(mContext).load(images.get(position).getUrl()).into(img);
        ImageView img2 = tag.likeImg;
        if(tag.imageObject.getIsLiked())
            img2.setImageResource(R.drawable.liked);
        else
            img2.setImageResource(R.drawable.like);
    }

    @NonNull
    private View inflateAndSetupImageItem(int position) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View imageItem = inflater.inflate(R.layout.image_item, null);
        ViewHolder tag = createTag(position, imageItem);
        setShareLongClickListenerForImage(tag.img);
        setClickListenerForLike(tag.img);
        setClickListenerForLikeButton(tag.likeImg);

        imageItem.setTag(tag);
        return imageItem;
    }

    @NonNull
    private ViewHolder createTag(int position, View imageItem) {
        ViewHolder tag = new ViewHolder();

        tag.imageObject = images.get(position);
        tag.img = (ImageView) imageItem.findViewById(R.id.image);
        tag.likeImg = (ImageView) imageItem.findViewById(R.id.like_image);
        tag.animateLikeImage = imageItem.findViewById(R.id.vBgLike);
        tag.animateLikeBackground = (ImageView) imageItem.findViewById(R.id.ivLike);
        tag.progressBar = (ProgressBar) imageItem.findViewById(R.id.progressBar);
        return tag;
    }


    private void setClickListenerForLike(ImageView img){
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewHolder vh = (ViewHolder)((View)view.getParent().getParent().getParent()).getTag();
                photoLike(vh);
            }
        });
    }
    
    private void setClickListenerForLikeButton(ImageView imgLike){
        imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewHolder vh = (ViewHolder)((View)view.getParent().getParent().getParent().getParent()).getTag();
                Log.e("asdasd", "" + vh.imageObject.getIsLiked());
                if(vh.imageObject.getIsLiked()) {
                    updateHeartButton(vh, false);
                    setImageObjectAsLiked(vh);
                    Utils.imageTracker(Utils.imageTrackingAction_disLike, Utils.imageTrackingLabel_heartIconClicked);

                }else {
                    updateHeartButton(vh, true);
                    resetImageObjectLiked(vh);
                    Utils.imageTracker(Utils.imageTrackingAction_like, Utils.imageTrackingLabel_heartIconClicked);
                }
            }
        });
    }

    private void updateHeartButton(final ViewHolder holder, boolean animated) {
        if (animated) {
                AnimatorSet animatorSet = new AnimatorSet();

                ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(holder.likeImg, "rotation", 0f, 360f);
                rotationAnim.setDuration(300);
                rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

                ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(holder.likeImg, "scaleX", 0.2f, 1f);
                bounceAnimX.setDuration(300);
                bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

                ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(holder.likeImg, "scaleY", 0.2f, 1f);
                bounceAnimY.setDuration(300);
                bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);
                bounceAnimY.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        holder.likeImg.setImageResource(R.drawable.liked);
                    }
                });

                animatorSet.play(rotationAnim);
                animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim);

                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        resetLikeAnimationState(holder);
                    }
                });

                animatorSet.start();
        }
        else {
            holder.likeImg.setImageResource(R.drawable.like);
        }
    }

    private void animatePhotoLike(final ViewHolder holder) {
        holder.animateLikeImage.setVisibility(View.VISIBLE);
        holder.animateLikeBackground.setVisibility(View.VISIBLE);

        holder.animateLikeImage.setScaleY(0.07f);
        holder.animateLikeImage.setScaleX(0.07f);
        holder.animateLikeImage.setAlpha(1f);
        holder.animateLikeBackground.setScaleY(0.1f);
        holder.animateLikeBackground.setScaleX(0.1f);

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(holder.animateLikeImage, "scaleY", 0.1f, 1f);
        bgScaleYAnim.setDuration(200);
        bgScaleYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(holder.animateLikeImage, "scaleX", 0.1f, 1f);
        bgScaleXAnim.setDuration(200);
        bgScaleXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(holder.animateLikeImage, "alpha", 1f, 0f);
        bgAlphaAnim.setDuration(200);
        bgAlphaAnim.setStartDelay(150);
        bgAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(holder.animateLikeBackground, "scaleY", 0.1f, 1f);
        imgScaleUpYAnim.setDuration(300);
        imgScaleUpYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(holder.animateLikeBackground, "scaleX", 0.1f, 1f);
        imgScaleUpXAnim.setDuration(300);
        imgScaleUpXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(holder.animateLikeBackground, "scaleY", 1f, 0f);
        imgScaleDownYAnim.setDuration(300);
        imgScaleDownYAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(holder.animateLikeBackground, "scaleX", 1f, 0f);
        imgScaleDownXAnim.setDuration(300);
        imgScaleDownXAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

        animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
        animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                resetLikeAnimationState(holder);
            }
        });
        animatorSet.start();
    }

    private void resetLikeAnimationState(ViewHolder holder) {
        holder.animateLikeImage.setVisibility(View.GONE);
        holder.animateLikeBackground.setVisibility(View.GONE);
    }

    private void setShareLongClickListenerForImage(ImageView image){
        image.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View arg0) {
                try {
                    Utils.shareImage((ImageView) arg0, activity);
                } catch (FileNotFoundException e) {
//                    Toast.makeText(arg0.getContext(), activity.getString(R.string.external_storage_is_uavailable), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
//                    Toast.makeText(arg0.getContext(), activity.getString(R.string.proble_with_external_storage), Toast.LENGTH_LONG).show();
                }

                return true;
            }
        });
    }

    private void photoLike(ViewHolder tag_item)
    {
        if (firstTap) {
            thisTime = SystemClock.uptimeMillis();
            tap_item = tag_item.imageObject.getUrl();
            firstTap = false;
        } else
        {
            if (tap_item == tag_item.imageObject.getUrl() ) {
                prevTime = thisTime;
                thisTime = SystemClock.uptimeMillis();

                if (thisTime > prevTime) {

                    if ((thisTime - prevTime) <= DOUBLE_CLICK_MAX_DELAY) {
                        likeImage(tag_item);
                        animatePhotoLike(tag_item);
                        updateHeartButton(tag_item, true);
                        firstTap = true;
                        Utils.imageTracker(Utils.imageTrackingAction_like, Utils.imageTrackingLabel_doubleClickOnImage);
                    } else {
                        firstTap = true;
                    }
                } else {
                    firstTap = true;
                }
            } else {
                firstTap = true;
            }
        }
    }


    private void likeImage(ViewHolder tag_item)
    {
        setImageObjectAsLiked(tag_item);
        doLikeAnimationsAndViews(tag_item);
    }

    private void setImageObjectAsLiked(ViewHolder tag)
    {
        Image image = tag.imageObject;
        image.setIsLiked(true);
        ClothServer cs = new RestClient().getClothServer();
        cs.notifyImageEvent(image.getId(), "like", new Callback<Message>() {
            @Override
            public void success(Message message, Response response) {
                Toast.makeText(mContext, "it was successfull" + message.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(mContext, "its failure" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void resetImageObjectLiked(ViewHolder tag)
    {
        Image image = tag.imageObject;
        image.setIsLiked(false);
        ClothServer cs = new RestClient().getClothServer();
        cs.notifyImageEvent(image.getId(), "dislike", new Callback<Message>() {
            @Override
            public void success(Message message, Response response) {
                Toast.makeText(mContext, "it was successfull" + message.getMessage(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(mContext, "its failure" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void doLikeAnimationsAndViews(ViewHolder tag_item) {
        tag_item.likeImg.setImageResource(R.drawable.liked);
        tag_item.likeImg.refreshDrawableState();
        animatePhotoLike(tag_item);
        updateHeartButton(tag_item,true);
    }
}
