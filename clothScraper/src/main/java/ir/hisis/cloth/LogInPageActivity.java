package ir.hisis.cloth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;

import ir.hisis.cloth.Utils.UtilUser;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.model.RestError;
import ir.hisis.cloth.uiapptemplate.view.FloatLabeledEditText;
import retrofit.client.Response;

public class LogInPageActivity extends Activity implements OnClickListener {

    private static final String TAG = LogInPageActivity.class.toString();
	public static final String LOGIN_PAGE_AND_LOADERS_CATEGORY = "ir.hisis.cloth.LogInPageAndLoadersActivity";
	public static final String DARK = "Dark";
	public static final String LIGHT = "Light";
    TextView login, register, forgot;
    FloatLabeledEditText UserText,PassText;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		String category = LIGHT;
        setContentView(R.layout.activity_login_page_light);
        UserText=(FloatLabeledEditText) findViewById(R.id.UserNameText);
        PassText=(FloatLabeledEditText) findViewById(R.id.PassText);
        login = (TextView) findViewById(R.id.login);
        register = (TextView) findViewById(R.id.register);
        forgot = (TextView) findViewById(R.id.forgot);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        forgot.setOnClickListener(this);
        reportLoginTracker("onCreate", null);
	}

	@Override
	public void onClick(View v) {
		if (v.getId()== R.id.login) {
            reportLoginTracker("login", null);
            UtilUser.login(UserText.getText().toString(), PassText.getText().toString(), getApplication(), new RestCallback<Boolean>() {
                @Override
                public void success(Boolean aBoolean, Response response) {
                    reportLoginTracker("login", "success");
                    authenticateUser();
                }

                @Override
                public void failure(RestError error) {
                    reportLoginTracker("login", "failure");
                    Toast.makeText(getApplicationContext(),error.getStrMessage(),Toast.LENGTH_SHORT).show();
                }
            });
		}
        if (v.getId()== R.id.register) {
            reportLoginTracker("register", null);
            Intent regi = new Intent(this,RegisterActivity.class);
            startActivity(regi);
            LogInPageActivity.this.finish();
        }
        if (v.getId()== R.id.forgot) {
            reportLoginTracker("forgot", null);
            Intent regi = new Intent(this,ForgotPassword.class);
            startActivity(regi);
            LogInPageActivity.this.finish();

        }
	}

    private void authenticateUser(){
        if(UtilUser.isAuthenticated()){
            finish();
        }
    }

    protected void reportLoginTracker(String action, String label) {
        clothApplication.tracker().setScreenName(getClass().getSimpleName());
        clothApplication.tracker().send(new HitBuilders.EventBuilder("LogIn_Activity", action)
                .setLabel(label)
                .build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        super.onStop();
    }
}