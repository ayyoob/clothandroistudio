package ir.hisis.cloth.fragments;

/**
 * Created by pooyafayyaz on 4/26/2015.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.etsy.android.grid.StaggeredGridView;
import com.google.android.gms.analytics.HitBuilders;

import ir.hisis.cloth.ImageAdapter;
import ir.hisis.cloth.R;
import ir.hisis.cloth.clothApplication;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.RestClient;
import ir.hisis.cloth.rest.model.ImageResponse;
import ir.hisis.cloth.rest.model.RestError;
import ir.hisis.cloth.rest.service.ClothServer;
import retrofit.client.Response;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.DefaultHeaderTransformer;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import uk.co.senab.actionbarpulltorefresh.library.viewdelegates.ViewDelegate;

public class MainFragment extends Fragment implements OnRefreshListener {

    private StaggeredGridView gridView;
    private ImageAdapter adapter;
    PullToRefreshLayout mPullToRefreshLayout;
    private int visibleThreshold = 15;
    private int currentPage = 0;
    private int previousTotal = 0;
    private boolean loading = true;
    private static int viewSize = 20;
    GestureDetector gestureDetector;
    boolean tapped;
    private String TAG = MainFragment.class.toString();

    public static final String ScrollingAction = "Scroll";
    public static final String ScrollingAction_down = "down";
    public static final String ScrollingAction_up = "up";
    public static final String ScrollingAction_down_addImages = "append";
    public static final String ScrollingAction_up_updateFromServer = "update";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container,
                false);
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.w("ayb" + TAG, "onActivityCreated");
        if(savedInstanceState!=null)
            Log.w("ayb"+TAG, savedInstanceState.getString(TAG));

        super.onActivityCreated(savedInstanceState);
        gridView = (StaggeredGridView) getView().findViewById(R.id.grid_view);

        mPullToRefreshLayout = (PullToRefreshLayout) getView().findViewById(R.id.ptr_layout);

        setupPullToRefresh();

        setupAdapterandImages();

    }

    private void setupPullToRefresh() {
        ActionBarPullToRefresh.from(getActivity())
                .options(Options.create().headerLayout(R.layout.default_header).build())
                .allChildrenArePullable()
                .useViewDelegate(StaggeredGridView.class, new ViewDelegate() {
                    @Override
                    public boolean isReadyForPull(View view, float v, float v2) {
                        View childView = gridView.getChildAt(0);
                        int top = (childView == null) ? 0 : childView.getTop();
                        return top >= 0;
                    }
                })
                .listener(this)
                .setup(mPullToRefreshLayout);

        DefaultHeaderTransformer transformer = (DefaultHeaderTransformer) mPullToRefreshLayout.getHeaderTransformer();
        transformer.getHeaderView().findViewById(R.id.ptr_text).
                setBackgroundColor(getResources().getColor(R.color.selector));
    }

    private void setupAdapterandImages() {
        adapter = new ImageAdapter(getActivity());
        gridView.setAdapter(adapter);

        gridView.setOnScrollListener(new HidingScrollListener());
        loadImageToAdapter();
    }



    private void loadImageToAdapter() {
        int lastImageId =0;
        if(adapter.getCount() > 0)
            lastImageId = adapter.getItem((adapter.getCount()-1)).getId();
        ClothServer cs = new RestClient().getClothServer();
        cs.getImages(viewSize, currentPage, false, false, lastImageId, new RestCallback<ImageResponse>() {
            @Override
            public void failure(RestError restError) {
                Log.i(TAG, "oh it failed");
            }

            @Override
            public void success(ImageResponse imageResponse, Response response) {
                Log.i(TAG, imageResponse.toString());
                adapter.addImage(imageResponse.getImages());

            }
        });

    }

    public void onRefreshStarted(View view) {
        refreshView();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.w(TAG, "onResume");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.w(TAG, "onStart");
    }

    private void refreshView() {

        scrollingAnalytics(ScrollingAction_down, ScrollingAction_down_addImages);

        ClothServer cs = new RestClient().getClothServer();
        cs.getImages(viewSize, currentPage, false, false, 0, new RestCallback<ImageResponse>() {
            @Override
            public void failure(RestError restError) {

                Log.i(TAG, "oh it failed");
            }

            @Override
            public void success(ImageResponse imageResponse, Response response) {
                adapter.resetImages(imageResponse.getImages());
                stopPullTorefresh();
            }
        });

    }

    private void stopPullTorefresh() {
        mPullToRefreshLayout.setRefreshComplete();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG, TAG);
    }

    private class HidingScrollListener implements AbsListView.OnScrollListener {
        int mLastFirstVisibleItem = 0;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            makeActionbarHidOnScrollDown(view);
            Log.w(TAG, "first: " + firstVisibleItem + " visiblecount: " + visibleItemCount + " total: " + totalItemCount +
                     "threshold is: " +visibleThreshold+ "previous is: "+ previousTotal +
                    " loading: " + loading + "current page: " + currentPage + "IMAGES COUNT: " + adapter.getCount());
            if (loading) {
                Log.w(TAG, "loading is true");
                if (totalItemCount > previousTotal) {
                    Log.w(TAG, "total is bigger than prev total");
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                Log.w(TAG, "going to load");
                scrollingAnalytics(ScrollingAction_up, ScrollingAction_up_updateFromServer);
                loadImageToAdapter();
                loading = true;
            }
        }

        private void makeActionbarHidOnScrollDown(AbsListView view) {
            if (view.getId() == gridView.getId()) {
                final int currentFirstVisibleItem = gridView.getFirstVisiblePosition();

                if (currentFirstVisibleItem > mLastFirstVisibleItem) {

                    ((ActionBarActivity) getActivity()).getSupportActionBar().hide();

                } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {

                    ((ActionBarActivity) getActivity()).getSupportActionBar().show();
                }

                mLastFirstVisibleItem = currentFirstVisibleItem;
            }

        }

    }

    protected void fragmentScreenAnalytics(String name){
        clothApplication.tracker().setScreenName("Fragment(" + name + ")");
        clothApplication.tracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    protected void scrollingAnalytics(String action, String label){
        clothApplication.tracker().setScreenName(getClass().getSimpleName());
        clothApplication.tracker().send(new HitBuilders.EventBuilder(ScrollingAction, action)
                .setLabel(label)
                .build());
    }
}
