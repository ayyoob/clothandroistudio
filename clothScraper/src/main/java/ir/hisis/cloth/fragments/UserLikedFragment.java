package ir.hisis.cloth.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by vandermonde on 6/13/2015.
 */
public class UserLikedFragment extends MainFragment {

    final String TAG = UserLikedFragment.class.getName();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.w("ayb"+TAG, "onActivityCreated");
        if(savedInstanceState!=null)
            Log.w("ayb" + TAG, savedInstanceState.getString(TAG));
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.w("ayb"+TAG, "onCreateView");
        if(savedInstanceState!=null)
            Log.w("ayb" + TAG, savedInstanceState.getString(TAG));
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onResume(){
        super.onResume();
        Log.w(TAG, "onResume");
        fragmentScreenAnalytics(TAG);
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.w(TAG, "onStart");
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG, TAG);
    }


}