package ir.hisis.cloth.Utils;


import com.flurry.android.FlurryAgent;

import java.util.Map;

/**
 * Created by misagh on 10/8/2015.
 */
public class FlurryAnalyticsHelper {

    public static void logEvent(String eventName, Map<String, String> eventParams, boolean timed) {
        FlurryAgent.logEvent(eventName, eventParams, timed);
    }

    public static void endTimedEvent(String eventName, Map<String, String> eventParams) {
        FlurryAgent.endTimedEvent(eventName, eventParams);
    }

    public static void logError(String errorId, String errorDescription, Throwable throwable) {
        FlurryAgent.onError(errorId, errorDescription, throwable);
    }

}
