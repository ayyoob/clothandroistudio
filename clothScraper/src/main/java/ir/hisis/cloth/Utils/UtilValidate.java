package ir.hisis.cloth.Utils;

/**
 * Created by vandermonde on 5/28/2015.
 */
public class UtilValidate {
    public static boolean isEmpty(String s) {
        return (s == null) || s.length() == 0;
    }
    public static <E> boolean isEmpty(CharSequence c) {
        return (c == null) || c.length() == 0;
    }

    public static boolean isNotEmpty(String s) {
        return (s != null) && s.length() > 0;
    }

    public static <E> boolean isNotEmpty(CharSequence c) {
        return ((c != null) && (c.length() > 0));
    }
}
