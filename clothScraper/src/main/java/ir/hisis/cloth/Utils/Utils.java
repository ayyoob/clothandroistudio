package ir.hisis.cloth.Utils;

import ir.hisis.cloth.R;
import ir.hisis.cloth.R.string;
import ir.hisis.cloth.clothApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.sax.StartElementListener;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.analytics.HitBuilders;

public class Utils {
	
	public static final String TAG = "utils";
	public static final String imageTrackingAction_share = "share";
	public static final String imageTrackingAction_like = "like";
	public static final String imageTrackingAction_disLike = "disLike";
	public static final String imageTrackingLabel_heartIconClicked = "icon";
	public static final String imageTrackingLabel_doubleClickOnImage = "2Click";

	public static void shareImage(ImageView view, Activity activity) throws FileNotFoundException,IOException {

		//TODO test if drawable is null
		Bitmap toShare = ((BitmapDrawable) view.getDrawable()).getBitmap();
		File file = generateRandomFileatDirectory(Environment.DIRECTORY_PICTURES);
		saveBitmapAsFile(toShare, file);
		
		Intent i = new Intent(Intent.ACTION_SEND, Uri.fromFile(file));
		i.setType("image/jpeg");
		i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
		activity.startActivity(Intent.createChooser(i, activity.getString(R.string.share_image_using)));

		imageTracker(imageTrackingAction_share, imageTrackingAction_share);
	}

	public static void imageTracker(String action, String label) {
		clothApplication.tracker().setScreenName(TAG);
		clothApplication.tracker().send(new HitBuilders.EventBuilder("image", action)
				.setLabel(label)
				.build());
	}

	/* Checks if external storage is available for read and write */
	public static boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}

	/* Checks if external storage is available to at least read */
	public static boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state)
				|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
	}

	public static void saveBitmapAsFile(Bitmap bitmapToShare, File file) throws FileNotFoundException,IOException  {
		 FileOutputStream out = new FileOutputStream(file);
		 Log.w(TAG, "file created");
		if( bitmapToShare.compress(Bitmap.CompressFormat.JPEG, 90, out) ){
			out.close();
		}
		
	}

	public static File generateRandomFileatDirectory(String directory) {

		File fileDir = Environment.getExternalStoragePublicDirectory(directory);
		fileDir.mkdirs();
		Log.wtf(TAG, "file dir: " + fileDir.getAbsolutePath());
		Random randomGenerator = new Random();
		int nom;
		File testF;
		do{
			nom = randomGenerator.nextInt();
			testF = new File(fileDir, "" + nom + ".png");
		}while (testF.exists());
		Log.wtf(TAG, "file: " + testF.getAbsolutePath());
		return testF;
	}
	
}
