package ir.hisis.cloth;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.squareup.okhttp.internal.Util;

import ir.hisis.cloth.Utils.UtilUser;
import ir.hisis.cloth.Utils.UtilUser.PasswordMatchException;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.model.RestError;
import ir.hisis.cloth.uiapptemplate.view.FloatLabeledEditText;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegisterActivity extends ActionBarActivity {
	
	private static String TAG = RegisterActivity.class.toString();

    TextView registerBtn;
    TextView whyBtn;
	FloatLabeledEditText userNameETxt;
    FloatLabeledEditText passwordETxt;
    FloatLabeledEditText repeatPasswordETxt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        setContentView(R.layout.activity_register);
//        TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//        String mPhoneNumber = tMgr.getLine1Number();
//        TelephonyManager telemamanger = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//        String getSimSerialNumber = telemamanger.getSimSerialNumber();
//        Log.e("as", "" + MyPhoneNumber);

		userNameETxt = (FloatLabeledEditText) findViewById(R.id.edtTxtUserNameRegister);
		passwordETxt = (FloatLabeledEditText) findViewById(R.id.edtTxtPasswordRegister);
		repeatPasswordETxt = (FloatLabeledEditText) findViewById(R.id.edtTxtRepeatPassword);
        whyBtn=(TextView) findViewById(R.id.whyButton);
		registerBtn = (TextView) findViewById(R.id.btnRegister);
		registerBtn.setOnClickListener(new RegBtnClickListener());

        whyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(RegisterActivity.this).create();
                alertDialog.setTitle("نام کاربری");
                alertDialog.setMessage("برای گرفتن تخفیف ها و اطلاعیه ها بهتر است شماره تلفن خود را وارد کنید");
                alertDialog.show();
            }
        });


        authenticateUser();

	}

	public class RegBtnClickListener implements View.OnClickListener{
		@Override
		public void onClick(View arg0) {
			try {
				UtilUser.registerUser(userNameETxt.getText().toString(),passwordETxt.getText().toString(),
                        repeatPasswordETxt.getText().toString(), getApplication(), new RestCallback<Boolean>() {
                            @Override
                            public void success(Boolean aBoolean, Response response) {
                                authenticateUser();
                            }

                            @Override
                            public void failure(RestError error) {
                                Toast.makeText(getApplicationContext(), "its an error" + error.getStrMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
			} catch (PasswordMatchException e) {
				Toast.makeText(RegisterActivity.this, R.string.passwords_doesnt_match, Toast.LENGTH_SHORT).show();
				return;
			}
		}
	}


    private void authenticateUser(){
        if(UtilUser.isAuthenticated()){
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        super.onStop();
    }
}
