package ir.hisis.cloth;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ir.hisis.cloth.fragments.AllImagesFragment;
import ir.hisis.cloth.fragments.MainFragment;
import ir.hisis.cloth.fragments.MostPopularImagesFragment;
import ir.hisis.cloth.fragments.UserLikedFragment;

/**
 * Created by Edwin on 15/02/2015.
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[];
    int NumbOfTabs;

    public TabsPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);
       this.Titles = mTitles;
       this.NumbOfTabs = mNumbOfTabsumb;
    }
    @Override
    public Fragment getItem(int index) {
        switch (index){
            case 0:
                return new AllImagesFragment();
            case 1:
                return new MostPopularImagesFragment();
            case 2:
                return new UserLikedFragment();
        }
        return  null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
