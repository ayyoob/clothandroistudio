package ir.hisis.cloth;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.google.android.gms.analytics.GoogleAnalytics;

public class ExploreActivity extends ActionBarActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		/*if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
		      finish();
		      return;
		    }*/

		setContentView(R.layout.explore_activity);
	/*	CategoryFragment cf = CategoryFragment.newInstance( "sss" );
		getSupportFragmentManager().beginTransaction().add(R.id.frmExplore, cf).commit();*/

        ExploreFragment ef = new ExploreFragment();
		getSupportFragmentManager().beginTransaction().add(R.id.frmExplore, ef).commit();

	}

	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
		super.onStop();
	}

}
