package ir.hisis.cloth;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;

public class CategoryActivity extends ActionBarActivity {

    MenuItem login_text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
/*	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.category, menu);
		
	}*/
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
        login_text = menu.findItem(R.id.viewusername);
        login_text.setTitle("پویا");
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
			case(R.id.action_explore):
				reportUiOpenedTracker("explore");
				startActivity(new Intent(getApplicationContext(), ExploreActivity.class));
				return true;
			case(R.id.action_settings):
				reportUiOpenedTracker("settings");
				startActivity(new Intent(getApplicationContext(), CategoryActivity.class));
				return true;
			}
		return super.onOptionsItemSelected(item);
	}

	private void reportUiOpenedTracker(String name) {
		clothApplication.tracker().setScreenName(getClass().getSimpleName());
		clothApplication.tracker().send(new HitBuilders.EventBuilder("ui", "open")
				.setLabel(name)
				.build());
	}

	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
		super.onStop();
	}
}
