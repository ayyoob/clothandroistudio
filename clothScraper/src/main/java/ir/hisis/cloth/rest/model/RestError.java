package ir.hisis.cloth.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vandermonde on 5/28/2015.
 */
public class RestError
{
    @SerializedName("code")
    private Integer code;

    @SerializedName("error_message")
    private String strMessage;

    public RestError(String strMessage)
    {
        this.strMessage = strMessage;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStrMessage() {
        return strMessage;
    }

    public void setStrMessage(String strMessage) {
        this.strMessage = strMessage;
    }

    @Override
    public String toString() {
        return "RestError{" + code + " " + strMessage + "}";
    }
}

