package ir.hisis.cloth.rest.model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vandermonde on 5/28/2015.
 */

public class Register {
    public static String TAG = Register.class.toString();

    @SerializedName("username")
    private String userName;

    @SerializedName("password")
    private String password;

    @SerializedName("passwordRepeat")
    private String passRepeat;

    public Register(String userName, String password){

        this.userName = userName;
        this.passRepeat = password;
        this.password = password;
        Log.i(TAG, "register sakhte shod");
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassRepeat() {
        return passRepeat;
    }

    public void setPassRepeat(String passRepeat) {
        this.passRepeat = passRepeat;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String toString(){
        return "Register{" + userName + ", " + password + "}";
    }
}
