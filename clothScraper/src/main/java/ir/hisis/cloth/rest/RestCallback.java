package ir.hisis.cloth.rest;

import ir.hisis.cloth.rest.model.RestError;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by vandermonde on 5/28/2015.
 */
public abstract class RestCallback<T> implements Callback<T>
{
    public abstract void failure(RestError restError);

    @Override
    public void failure(RetrofitError error)
    {
        RestError restError = (RestError) error.getBodyAs(RestError.class);

        if (restError != null)
            failure(restError);
        else
        {
            failure(new RestError(error.getMessage()));
        }
    }
}

