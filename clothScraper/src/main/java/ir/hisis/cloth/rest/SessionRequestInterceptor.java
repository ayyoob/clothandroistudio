package ir.hisis.cloth.rest;

import ir.hisis.cloth.Utils.UtilUser;
import retrofit.RequestInterceptor;

/**
 * Created by vandermonde on 8/17/15.
 */
public class SessionRequestInterceptor implements RequestInterceptor
{
    @Override
    public void intercept(RequestFacade request)
    {
        if (UtilUser.isAuthenticated())
        request.addHeader(UtilUser.AUTH_TOKEN_LABEL, UtilUser.getAuthenticationToken());
    }
}