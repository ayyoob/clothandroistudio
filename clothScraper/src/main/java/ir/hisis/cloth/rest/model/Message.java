package ir.hisis.cloth.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vandermonde on 6/15/2015.
 */
public class Message {


    @SerializedName("message")
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
