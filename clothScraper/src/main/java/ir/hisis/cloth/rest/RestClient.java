package ir.hisis.cloth.rest;

import ir.hisis.cloth.rest.service.ClothServer;
import retrofit.RestAdapter;

/**
 * Created by vandermonde on 5/28/2015.
 */
public class RestClient {
    public static final String TAG = RestClient.class.toString();
    public static final String BASE_URL = "http://188.166.109.225:8080";
    private ClothServer clothServer;

    public RestClient()
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(new SessionRequestInterceptor())
                .build();
        clothServer = restAdapter.create(ClothServer.class);
    }

    public ClothServer getClothServer()
    {
        return clothServer;
    }

}
