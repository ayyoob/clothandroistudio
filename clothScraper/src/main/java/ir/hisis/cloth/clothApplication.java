package ir.hisis.cloth;

import android.app.Application;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;

/**
 * Created by misagh on 10/8/2015.
 */
public class clothApplication extends Application {

    private static final String FLURRY_API_KEY = "ZK6PQ82BGRM8VBMFT3MM";

    /**
     * The Analytics singleton. The field is set in onCreate method override when the application
     * class is initially created.
     */
    private static GoogleAnalytics analytics;

    /**
     * The default app tracker. The field is from onCreate callback when the application is
     * initially created.
     */
    private static Tracker tracker;

    /**
     * Access to the global Analytics singleton. If this method returns null you forgot to either
     * set android:name="&lt;this.class.name&gt;" attribute on your application element in
     * AndroidManifest.xml or you are not setting this.analytics field in onCreate method override.
     */
    public static GoogleAnalytics analytics() {
        return analytics;
    }

    /**
     * The default app tracker. If this method returns null you forgot to either set
     * android:name="&lt;this.class.name&gt;" attribute on your application element in
     * AndroidManifest.xml or you are not setting this.tracker field in onCreate method override.
     */
    public static Tracker tracker() {
        return tracker;
    }

    public clothApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        analytics = GoogleAnalytics.getInstance(this);
        tracker = analytics.newTracker(R.xml.app_tracker);

        // Provide unhandled exceptions reports. Do that first after creating the tracker
        tracker.enableExceptionReporting(true);
/*
        // Enable Remarketing, Demographics & Interests reports
        // https://developers.google.com/analytics/devguides/collection/android/display-features
        tracker.enableAdvertisingIdCollection(true);
*/
        // Enable automatic activity tracking for your app
        tracker.enableAutoActivityTracking(true);

        ParseCrashReporting.enable(this);
        Parse.initialize(this, "UKSWRuY43bMapT8yBr2JGinBVv9MVJqSj87BZK2I", "yCNEx7S5vzPXjAlYesrdRaOXqiRWbbylivn3mA4L");
        ParseInstallation.getCurrentInstallation().saveInBackground();

/*
        // configure Flurry
        // init Flurry
        FlurryAgent.setLogEnabled(false);
        FlurryAgent.init(this, FLURRY_API_KEY);
*/

    }
}