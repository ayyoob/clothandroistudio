package ir.hisis.cloth;

import java.util.ArrayList;


public class CategoryGroup {
	
	String groupName;
	final ArrayList<Category> categories = new ArrayList<CategoryGroup.Category>();
	
	
	public void setName(String name){
		groupName= name;
	}
	
	public String getName(){
		return groupName;
	}
	
	public Category getCategory(int index){
		return categories.get(index);
	}
	
	public void addCategory(Category category){
		categories.add(category);
	}
	
	public int getchildrenCount(){
		return categories.size();
	}
	
	public static class Category{
		String catName;
		int catCount;
		
		public void setName(String name){
			catName = name;
		}
		
		public String getName(){
			return catName;
		}
		
		public void setCategoryCount(int count){
			catCount = count;
		}
		
		public int getCategoryCount(){
			return catCount;
		}
	}
}
