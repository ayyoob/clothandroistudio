package ir.hisis.cloth;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ir.hisis.cloth.uiapptemplate.view.AnimatedExpandableListView;
import ir.hisis.cloth.uiapptemplate.view.AnimatedExpandableListView.AnimatedExpandableListAdapter;

public class ExpandableListViewActivity extends ActionBarActivity {

	private AnimatedExpandableListView listView;
	private ExampleAdapter adapter;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_expandable_list_view);

        Display newDisplay = getWindowManager().getDefaultDisplay();


        int width_rtl = newDisplay.getWidth();

        List<GroupItem> items = new ArrayList<GroupItem>();

		// Populate our list with groups and it's children
		for (int i = 1; i < 100; i++) {
			GroupItem item = new GroupItem();

			item.title = "Expand this item " + i;

			for (int j = 0; j < i; j++) {
				ChildItem child = new ChildItem();
				child.title = "Expanded " + j;
				//child.hint = "Too awesome";

				item.items.add(child);
			}

			items.add(item);
		}

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		adapter = new ExampleAdapter(this);

        adapter.setData(items);

		listView = (AnimatedExpandableListView) findViewById(R.id.list_view);


		listView.setAdapter(adapter);

		// In order to show animations, we need to use a custom click handler
		// for our ExpandableListView.
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean  onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                int id_click = v.getId();
                switch (id_click)
                {
                    case (R.id.textTitle):
                        Log.e("", ""+ id);
                }
                if (listView.isGroupExpanded(groupPosition)) {
                    listView.collapseGroupWithAnimation(groupPosition);
                } else {
                    listView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }

        });
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
                return false;
            }
        });

		// Set indicator (arrow) to the right
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		//Log.v("width", width + "");
		Resources r = getResources();
		int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				50, r.getDisplayMetrics());
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			listView.setIndicatorBounds(width - px, width);
		} else {
			listView.setIndicatorBoundsRelative(width - px, width);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private static class GroupItem {
        boolean is_liked;
        TextView like;
		String title;
		List<ChildItem> items = new ArrayList<ChildItem>();
	}

	private static class ChildItem {
        boolean is_liked;
        TextView like;
		String title;
		//String hint;
	}

	private static class ChildHolder {
        boolean is_liked;
        TextView like;
		TextView title;
		//TextView hint;
	}

	private static class GroupHolder {
        boolean is_liked;
        TextView like;
		TextView title;
	}

	/**
	 * Adapter for our list of {@link GroupItem}s.
	 */
	private class ExampleAdapter extends AnimatedExpandableListAdapter {
		private LayoutInflater inflater;

		private List<GroupItem> items;

		public ExampleAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		public void setData(List<GroupItem> items) {
			this.items = items;
		}

		@Override
		public ChildItem getChild(int groupPosition, int childPosition) {
			return items.get(groupPosition).items.get(childPosition);
		}

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
		public View getRealChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
			final ChildHolder holder;
			ChildItem item = getChild(groupPosition, childPosition);
			if (convertView == null) {
                Log.e("","is null" );
				holder = new ChildHolder();
				convertView = inflater.inflate(R.layout.list_item, parent,
						false);
                holder.like = (TextView) convertView
                        .findViewById(R.id.child_like_image);

				holder.title = (TextView) convertView
						.findViewById(R.id.textTitle);
				/*holder.hint = (TextView) convertView
						.findViewById(R.id.textHint);*/
				convertView.setTag(holder);
			} else {
				holder = (ChildHolder) convertView.getTag();
			}
			holder.title.setText(item.title);
            holder.like.setText(R.string.fontello_heart_empty);
            holder.is_liked = false ;

            holder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.is_liked) {
                        holder.like.setText(R.string.fontello_heart_empty);
                        holder.is_liked = !holder.is_liked;
                    }
                    else
                    {
                        holder.is_liked = !holder.is_liked;
                        holder.like.setText(R.string.fontello_heart_full);
                    }

                }

            });
			return convertView;
		}

		@Override
		public int getRealChildrenCount(int groupPosition) {
			return items.get(groupPosition).items.size();
		}

		@Override
		public GroupItem getGroup(int groupPosition) {
			return items.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return items.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			final GroupHolder holder;
			GroupItem item = getGroup(groupPosition);
			if (convertView == null) {
				holder = new GroupHolder();
				convertView = inflater.inflate(R.layout.group_item, parent,
						false);
                holder.like = (TextView) convertView
                        .findViewById(R.id.group_like_image);
				holder.title = (TextView) convertView
						.findViewById(R.id.textTitle);
				convertView.setTag(holder);
			} else {
				holder = (GroupHolder) convertView.getTag();
			}
			holder.title.setText(item.title);
            holder.like.setText(R.string.fontello_heart_full);
            holder.is_liked = true ;
            holder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(holder.is_liked) {
                        holder.like.setText(R.string.fontello_heart_empty);
                        holder.is_liked = !holder.is_liked;
                    }
                    else
                    {
                        holder.is_liked = !holder.is_liked;
                        holder.like.setText(R.string.fontello_heart_full);
                    }
                }
            });
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}
	}
}